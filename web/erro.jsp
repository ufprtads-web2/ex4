<%-- 
    Document   : erro
    Created on : Sep 17, 2018, 11:08:07 PM
    Author     : cassiano
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP ERROR PAGE</title>
    </head>
    <body>        
        <%
            if (request.getAttribute("errorMsg") == null) { 
                out.println("Standard Error MSG");
            } else {
                out.println("<h1> Mensagem:"+request. getAttribute("errorMsg")+"</h1>");
                out.println("<a href="+ request.getAttribute("errorPage")+">Retornar</a>");
            }
        %>           
            
    </body>
</html>
