<%-- 
    Document   : portal
    Created on : Sep 17, 2018, 10:36:33 PM
    Author     : cassiano
--%>

<%@page import="com.ufpr.tads.web2.beans.LoginBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%         
    LoginBean usuario = (LoginBean)session.getAttribute("login");        
    if (usuario == null){
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/erro.jsp");                
        request.setAttribute("errorMsg","Deu Ruim");
        request.setAttribute("errorPage","index.html");
        rd.forward(request, response);
    }
%>
<jsp:useBean id="configuracao" class="com.ufpr.tads.web2.beans.ConfigBean"
scope="application"/>
<jsp:useBean id="login" class="com.ufpr.tads.web2.beans.LoginBean"
scope="session"/>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Portal JSP</title>
    </head>
    <body>
    <h2>Dados da Sessão</h2>
    Bem vindo, <jsp:getProperty name="login" property="login" /><br />
    <a href="LogoutServlet">Logout</a>
        <footer>
            Em caso de problemas contactar o administrador: <jsp:getProperty name="configuracao" property="email" />
        </footer>
    </body>
</html>
